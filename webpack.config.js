const { join } = require('path');

const webpack = require('webpack');

const merge = require('./webpack.base.config');

module.exports = merge({
  mode: "development",
  devtool: "eval-source-map",
  resolve: {
    extensions: ['.js', '.jsx'],
    modules: ['app', 'node_modules']
  },
  output: {},
  devServer: {
    hot: true,
    contentBase: './dist',
    open: true,
  },
  module: {
    rules: [
      {
        test: /\.less$/,
        include: /node_modules/,
        loaders: [
          'style-loader',
          'css-loader',
          {
            loader: 'less-loader',
            options: {
              javascriptEnabled: true
            }
          }
        ]
      },
      {
        test: /\.less$/,
        exclude: /node_modules/,
        loaders: [
          'style-loader',
          'css-loader?modules&camelCase&importLoaders=1&localIdentName=[local]-[hash:8]',
          'postcss-loader',
          {
            loader: 'less-loader',
            options: {
              javascriptEnabled: true,
            }
          }
        ]
      },
      {
        test: /\.css$/,
        loaders: [
          'style-loader',
          'css-loader',
        ]
      },
    ]
  },
  plugins: [
    new webpack.NamedModulesPlugin(),
    new webpack.HotModuleReplacementPlugin(),
  ]
});