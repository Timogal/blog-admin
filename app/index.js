import React from 'react';
import { render } from 'react-dom';
import { HashRouter, Switch, Route } from 'react-router-dom';

import 'babel-polyfill';

import App from 'containers/App';
import LoginPage from "containers/LoginPage/async";

render(
  (
    <HashRouter>
      <Switch>
        <Route path="/login" component={LoginPage} />
        <Route path="/" component={App} />
      </Switch>
    </HashRouter>
  ),
  document.getElementById('app')
);