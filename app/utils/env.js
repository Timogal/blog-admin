const isProd = process.env.NODE_ENV === 'production';

const apiBase = isProd ? 'https://api.zhyui.com' : 'http://localhost:8088';
const uploadURL = isProd ? 'https://upload-z2.qiniup.com' : 'http://upload-z2.qiniup.com';

export {
  apiBase,
  uploadURL,
};