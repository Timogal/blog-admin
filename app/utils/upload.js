import axios from 'axios';
import Api from './api';

import { uploadURL } from './env';

const prefix = 'images/';

function uuid() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    const r = Math.random() * 16 | 0;
    const v = c === 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}

/**
 *
 * @param {File} file
 * @returns {Promise<string>}
 */
async function upload(file) {
  const suffixIndex = file.name.lastIndexOf('.');
  const suffix = file.name.substring(suffixIndex);
  const key = prefix + uuid() + suffix;
  const data = new FormData();
  const { data: { token } } = await Api.get(`/uploads`);
  data.append("key", key);
  data.append("file", file);
  data.append("token", token);
  await axios.post(uploadURL, data);
  return '/' + key;
}

export default upload;