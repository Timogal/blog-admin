import axios from 'axios';
import { message } from 'antd';

import { apiBase } from './env';

const Api = axios.create({
  baseURL: apiBase
});

Api.interceptors.request.use((config) => {
  if (config._noCheck) {
    return config;
  }
  const access_token = localStorage.getItem('access_token');
  if (!access_token) {
    throw new Error('Access token is missing');
  }
  config.headers.Authorization = `Bearer ${access_token}`;
  return config;
}, (error) => {
  message.error(error.message);
  return Promise.reject(error);
});

export default Api;