import React from "react";

import styles from './NotFound.less';

import notFoundImage from './404.jpg';

function NotFound() {
  return (
    <div className={styles.notFound}>
      <img src={notFoundImage} alt="Page Not Found" />
    </div>
  );
}

export default NotFound;