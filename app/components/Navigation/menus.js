export default [
  {
    key: 'articles',
    title: '文章',
    path: '/posts',
    icon: 'file-markdown'
  },
  {
    key: 'links',
    title: '友情链接',
    path: '/links',
    icon: 'link'
  }
];