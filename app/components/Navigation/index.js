import React, { Component } from "react";
import PropTypes from "prop-types";
import { Link } from 'react-router-dom';

import { Menu, Icon } from 'antd';

import menus from './menus';

class Navigation extends Component {

  render() {
    return (
      <Menu theme={'dark'}>
        {
          menus.map(({ title, icon, key, path }) =>
            (
              <Menu.Item key={key}>
                <Link to={path}>
                  {
                    icon &&
                    <Icon type={icon} />
                  }
                  <span>{title}</span>
                </Link>
              </Menu.Item>
            )
          )
        }
      </Menu>
    );
  }
}

Navigation.propTypes = {};

Navigation.defaultProps = {};

export default Navigation;