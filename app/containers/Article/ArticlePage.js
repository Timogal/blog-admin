import React from 'react';
import { Button, Table, Breadcrumb, Popconfirm, message } from 'antd';
import { Link } from 'react-router-dom';
import moment from 'moment';

import Api from 'utils/api';

class ArticlePage extends React.Component {
  state = {
    loading: false,
    items: null,
    page: 1,
  };

  componentWillMount() {
    this.loadArticles();
  }

  async loadArticles() {
    const { page, loading } = this.state;
    if (loading) {
      return;
    }
    this.setState({ loading: true });
    try {
      const { data } = await Api.get('/articles/s', {
        params: { page }
      });
      this.setState({ loading: false, items: data.items });
    } catch (e) {
      this.setState({ loading: false });
    }
  }

  deleteArticle = async (id) => {
    try {
      await Api.delete(`/articles/${id}`);
      this.loadArticles();
    } catch (e) {
      message.error('删除失败');
    }
  };

  renderOperations = (value, item, index) => {
    return (
      <Breadcrumb separator="|">
        <Breadcrumb.Item>
          <Link to={`/posts/${item.id}`}>编辑</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>
          <Popconfirm title="确定要删除该文章吗?" onConfirm={() => this.deleteArticle(item.id)}>
            <a>删除</a>
          </Popconfirm>
        </Breadcrumb.Item>
      </Breadcrumb>
    );
  };

  render() {
    const { loading, items } = this.state;
    const columns = [{
      title: 'ID',
      dataIndex: 'id',
      key: 'id',
    }, {
      title: '标题',
      dataIndex: 'title',
      key: 'title',
    }, {
      title: '摘要',
      dataIndex: 'remark',
      key: 'remark',
    }, {
      title: '浏览数',
      dataIndex: 'views',
      key: 'views',
    }, {
      title: '上次更新',
      dataIndex: 'lastModified',
      key: 'lastModified',
      render(value) {
        return <span>{moment(value).format('YYYY-MM-DD HH:mm')}</span>
      },
    }, {
      title: '操作',
      key: 'op',
      dataIndex: 'id',
      render: this.renderOperations
    }];
    return (
      <div>
        <div style={{ marginBottom: '24px' }}>
          <Link to="/posts/create"><Button type="primary">添加</Button></Link>
        </div>
        <Table
          rowKey="id"
          dataSource={items}
          loading={loading}
          columns={columns}
          pagination={false}
          bordered
        />
      </div>
    );
  }
}

export default ArticlePage;