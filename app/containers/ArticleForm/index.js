import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./ArticleForm'),
  loading: () => null
});