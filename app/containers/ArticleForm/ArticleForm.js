import React from 'react';
import PropTypes from 'prop-types';

import { Form, Input, Select, Button, message } from 'antd';
import ReactMde from 'react-mde';
import Showdown from 'showdown';

import Api from 'utils/api';
import upload from 'utils/upload';

import styles from './form.less';
import 'react-mde/lib/styles/css/react-mde-all.css';

const Item = Form.Item;
const SelectOption = Select.Option;

class ArticleForm extends React.Component {
  constructor(props) {
    super(props);
    this.converter = new Showdown.Converter({
      tables: true,
      simplifiedAutoLink: true,
      strikethrough: true,
      tasklists: true,
    });
    const { id } = props.match.params;
    this.isAddMode = id === 'create';
  }

  isAddMode = false;
  converter = null;

  state = {
    editorState: null,
    fileIndex: 1,
    bgFile: null,
    saving: false,
    detail: null,
  };

  async componentWillMount() {
    if (this.isAddMode) {
      return;
    }
    const { id } = this.props.match.params;
    const { data } = await Api.get(`/articles/${id}`);
    this.setState({
      detail: data,
      editorState: {
        markdown: data.markdown,
      }
    });
  }

  onBgChange = (e) => {
    const file = e.target.files[0];
    this.setState(({ fileIndex }) => {
      return {
        fileIndex: ++fileIndex,
        bgFile: file
      }
    })
  };

  handleValueChange = (mdeState) => {
    this.setState({ editorState: mdeState });
  };

  saveArticle = () => {
    const { saving, editorState, bgFile } = this.state;
    if (saving) {
      return;
    }
    if (!editorState || !editorState.markdown) {
      message.warn('请填写文章内容');
      return;
    }
    const { validateFields } = this.props.form;
    validateFields(async (errors, values) => {
      if (errors) {
        return;
      }
      const markdown = editorState.markdown;
      const html = this.converter.makeHtml(editorState.markdown);
      this.setState({ saving: true });
      try {
        let bg = undefined;
        if (bgFile) {
          bg = await upload(bgFile);
        }
        const data = {
          ...values,
          content: html,
          backgroundImage: bg,
          markdown,
        };
        if (this.isAddMode) {
          await Api.post('/articles', data);
        } else {
          const { id } = this.props.match.params;
          await Api.patch(`/articles/${id}`, data);
        }
        this.setState({ saving: true }, () => {
          message.success('保存成功');
        });
      } catch (e) {
        this.setState({ saving: false }, () => {
          message.error('保存失败');
        });
      }
    });
  };

  render() {
    const { editorState, fileIndex, detail } = this.state;
    const wrapperCol = { xs: 24, sm: 18, md: 16 };
    const contentWrapperCol = { xs: 24, sm: 18, md: 20 };
    const labelCol = { xs: 24, sm: 6, md: 4 };
    const { getFieldDecorator } = this.props.form;
    return (
      <Form hideRequiredMark={false} className={styles.container}>
        <Item label="标题" wrapperCol={wrapperCol} labelCol={labelCol}>
          {getFieldDecorator('title', {
            initialValue: detail ? detail.title : '',
            rules: [
              { required: true, message: '请填写标题' }
            ]
          })(
            <Input size="large" />
          )}
        </Item>
        <Item label="分类" wrapperCol={wrapperCol} labelCol={labelCol}>
          {getFieldDecorator('categories', {
            initialValue: detail ? detail.categories.map(item => item.name) : [],
            rules: [
              { required: true, message: '请填写分类' }
            ]
          })(
            <Select size="large" mode="tags" />
          )}
        </Item>
        <Item label="标签" wrapperCol={wrapperCol} labelCol={labelCol}>
          {getFieldDecorator('tags', {
            initialValue: detail ? detail.tags.map(item => item.name) : [],
          })(
            <Select size="large" mode="tags" />
          )}
        </Item>
        <Item label="背景图" wrapperCol={wrapperCol} labelCol={labelCol}>
          <Input
            id={`bg-${fileIndex}`}
            type="file"
            size="large"
            accept="image/jpeg,image/png"
            style={{ padding: '0' }}
            onChange={this.onBgChange}
          />
        </Item>
        <Item label="内容" wrapperCol={contentWrapperCol} labelCol={labelCol}>
          <div className={styles.editor}>
            <ReactMde
              onChange={this.handleValueChange}
              editorState={editorState}
              generateMarkdownPreview={markdown => Promise.resolve(this.converter.makeHtml(markdown))}
            />
          </div>
        </Item>
        <Item label=" " colon={false} wrapperCol={contentWrapperCol} labelCol={labelCol}>
          <Button
            size="large"
            type="primary"
            htmlType="button"
            onClick={this.saveArticle}
          >
            保存
          </Button>
        </Item>
      </Form>
    );
  }
}

ArticleForm.propTypes = {
  form: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
};

export default Form.create()(ArticleForm);