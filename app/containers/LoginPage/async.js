import Loadable from 'react-loadable';

export default Loadable({
  loading: () => null,
  loader: () => import('./index')
});