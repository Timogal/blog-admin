import React, { Component } from "react";
import PropTypes from 'prop-types';
import { Form, Input, Button, message } from 'antd';
import { withRouter } from 'react-router-dom';

import Api from 'utils/api';

import styles from './Login.less';

const auth = 'Basic cHVibGlzaGVyOnRoaXNpc2FzZWNyZXQ=';

class LoginPage extends Component {

  doLogin = () => {
    const { history } = this.props;
    const { validateFields } = this.props.form;
    validateFields(async (errors, values) => {
      if (errors) {
        return;
      }
      try {
        const { data } = await Api.post('/oauth/token', null, {
          _noCheck: true,
          headers: {
            Authorization: auth
          },
          params: {
            grant_type: 'password',
            ...values,
          }
        });
        const accessToken = data.access_token;
        localStorage.setItem('access_token', accessToken);
        history.push('/posts');
      } catch (e) {
        message.error("用户名或密码错误");
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div className={styles.wrapper}>
        <Form className={styles.frame}>
          <div className={styles.head}>
            <h2>blog.zhyui.com后台管理</h2>
          </div>
          <Form.Item>
            {
              getFieldDecorator('username', {
                rules: [
                  { required: true, message: '请输入用户名' },
                  { whitespace: true }
                ]
              })(
                <Input size="large" placeholder="用户名" />
              )
            }
          </Form.Item>
          <Form.Item>
            {
              getFieldDecorator('password', {
                rules: [
                  { required: true, message: '请输入密码' },
                  { whitespace: true }
                ]
              })(
                <Input size="large" placeholder="密码" type="password" />
              )
            }
          </Form.Item>
          <Form.Item style={{ marginBottom: 0 }}>
            <Button
              htmlType="button"
              type="primary"
              size="large"
              onClick={this.doLogin}
              style={{ width: '100%' }}
            >
              登录
            </Button>
          </Form.Item>
        </Form>
      </div>
    );
  }
}

LoginPage.propTypes = {
  form: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

const WrappedLogin = Form.create()(LoginPage);

export default withRouter(WrappedLogin);