import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./Link'),
  loading: () => null
});