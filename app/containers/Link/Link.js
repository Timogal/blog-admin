import React, { Component } from "react";
import {
  Table,
  Breadcrumb,
  Button,
  Modal,
  Form,
  Input,
  message,
  Popconfirm
} from 'antd';

import Api from 'utils/api';

class Link extends Component {
  state = {
    loading: false,
    links: [],
    modalShow: false,
    modalMode: null,
    currentEditLink: null,
    textValue: '',
    linkValue: '',
  };

  componentWillMount() {
    this.loadLinks();
  }

  async loadLinks() {
    this.setState({
      loading: true,
    });
    const { data: links } = await Api.get('/friend-links');
    this.setState({
      loading: false,
      links,
    });
  }

  saveLink = async () => {
    const { textValue, linkValue, modalMode, currentEditLink } = this.state;
    if (!textValue || !linkValue) {
      message.warn('请填写数据');
      return;
    }
    if (modalMode === 'add') {
      await Api.post('/friend-links', null, {
        params: {
          text: textValue,
          link: linkValue
        }
      })
    } else {
      await Api.patch(`/friend-links/${currentEditLink.id}`, null, {
        params: {
          text: textValue,
          link: linkValue
        }
      })
    }
    this.setState({
      modalShow: false,
      modalMode: null,
      currentEditLink: null,
    }, this.loadLinks)
  };

  deleteLink = (id) => {
    Api.delete(`/friend-links/${id}`)
      .then(() => this.loadLinks())
      .catch(error => message.error("删除失败:" + error.message));
  };

  renderOperations = (value, item, index) => {
    return (
      <Breadcrumb separator="|">
        <Breadcrumb.Item>
          <a
            onClick={() => this.setState({
              modalShow: true,
              modalMode: 'edit',
              currentEditLink: item,
              linkValue: item.link,
              textValue: item.text,
            })}
          >
            编辑
          </a>
        </Breadcrumb.Item>
        <Breadcrumb.Item>
          <Popconfirm title="确定要删除该链接吗?" onConfirm={() => this.deleteLink(item.id)}>
            <a>删除</a>
          </Popconfirm>
        </Breadcrumb.Item>
      </Breadcrumb>
    );
  };

  renderModal() {
    const { modalShow, modalMode, currentEditLink, textValue, linkValue } = this.state;
    if (!modalShow) {
      return null;
    }
    return (
      <Modal
        visible={modalShow}
        title={modalMode === 'edit' ? '编辑' : '添加'}
        onCancel={() => this.setState({ modalShow: false, modalMode: null, currentEditLink: null })}
        onOk={this.saveLink}
      >
        <Form>
          <Form.Item>
            <Input
              placeholder="名称"
              value={textValue}
              onChange={(e) => this.setState({ textValue: e.target.value })}
            />
          </Form.Item>
          <Form.Item>
            <Input
              placeholder="链接"
              value={linkValue}
              onChange={(e) => this.setState({ linkValue: e.target.value })}
            />
          </Form.Item>
        </Form>
      </Modal>
    );
  }

  render() {
    const { loading, links } = this.state;
    const columns = [
      {
        title: 'ID',
        dataIndex: 'id',
        key: 'id',
      }, {
        title: '名称',
        dataIndex: 'text',
        key: 'text',
      }, {
        title: '链接',
        dataIndex: 'link',
        key: 'link',
        render(value) {
          return <a href={value} target="_blank">{value}</a>
        }
      }, {
        title: '操作',
        key: 'op',
        dataIndex: 'id',
        render: this.renderOperations
      }
    ];
    return (
      <div>
        <div style={{ marginBottom: '24px' }}>
          <Button
            onClick={() => this.setState({ modalShow: true, modalMode: 'add' })}
            type="primary"
          >
            添加
          </Button>
        </div>
        <Table
          rowKey="id"
          dataSource={links}
          loading={loading}
          columns={columns}
          pagination={false}
          bordered
        />
        {this.renderModal()}
      </div>
    );
  }
}

Link.propTypes = {};

Link.defaultProps = {};

export default Link;