import React from 'react';
import { Switch, Route, Redirect, withRouter } from 'react-router-dom';
import { Layout, Spin } from 'antd';

import Navigation from 'components/Navigation';

import NotFound from 'components/NotFound';

import Api from 'utils/api';

import LinkPage from '../Link';
import ArticlePage from '../Article';
import ArticleForm from '../ArticleForm';

import styles from './App.less';
import PropTypes from "prop-types";

const { Header, Footer, Sider, Content } = Layout;

class App extends React.Component {
  state = {
    checking: true
  };

  async componentWillMount() {
    const { match, history } = this.props;
    if (match.path !== '/login') {
      const accessToken = localStorage.getItem('access_token');
      if (!accessToken) {
        return history.push('/login');
      }
      try {
        await Api.get('/oauth/check_token', {
          _noCheck: true,
          params: {
            token: accessToken
          }
        });
        this.setState({ checking: false });
      } catch (e) {
        return history.push('/login');
      }
    } else {
      this.setState({
        checking: false
      });
    }
  }

  logout = () => {
    const { history } = this.props;
    localStorage.removeItem('access_token');
    history.push('/login');
  };

  render() {
    const { checking } = this.state;
    if (checking) {
      return (
        <div className={styles.loading}>
          <div><Spin spinning /></div>
        </div>
      );
    }
    return (
      <Layout className={styles.main}>
        <Header>
          <div style={{ float: 'right' }}>
            <a onClick={this.logout}>登出</a>
          </div>
        </Header>
        <Layout>
          <Sider>
            <Navigation />
          </Sider>
          <Layout>
            <Content className={styles.content}>
              <Switch>
                <Route path="/links" component={LinkPage} />
                <Route exact path="/posts" component={ArticlePage} />
                <Route path="/posts/:id" component={ArticleForm} />
                <Redirect path="/" to="/posts" />
                <Route component={NotFound} />
              </Switch>
            </Content>
            <Footer>
              Blog Admin
            </Footer>
          </Layout>
        </Layout>
      </Layout>
    );
  }
}

App.propTypes = {
  match: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default withRouter(App);