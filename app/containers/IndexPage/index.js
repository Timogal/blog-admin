import React from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';

import { Spin } from 'antd';

import Api from 'utils/api';

import styles from './Index.less';

class App extends React.Component {
  async componentWillMount() {
    const { match, history, location } = this.props;
  }

  render() {
    return (
      <div className={styles.root}>
        <Spin spinning={true} size="large" />
      </div>
    );
  }
}

App.propTypes = {
  match: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default withRouter(App);