const { join } = require('path');

const webpack = require('webpack');

const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = function merge(another) {
  return {
    mode: another.mode || "development",
    devtool: another.devtool,
    entry: {
      app: './app/index'
    },
    output: {
      path: join(__dirname, 'dist'),
      filename: another.output.filename || '[name].js',
      chunkFilename: another.output.chunkFilename || '[name].bundle.js',
      publicPath: '/',
    },
    devServer: another.devServer,
    resolve: {
      extensions: ['.js', '.jsx'],
      modules: ['app', 'node_modules']
    },
    module: {
      rules: [
        {
          test: /\.jsx?$/,
          exclude: /node_modules/,
          loader: 'babel-loader'
        },
        {
          test: /\.(png|jpg|gif|ico|woff|woff2|ttf|svg)$/,
          loader: 'file-loader'
        }
      ].concat(another.module.rules)
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: './app/index.html'
      }),

      new webpack.DefinePlugin({
        NODE_ENV: JSON.stringify(process.env.NODE_ENV)
      }),
    ].concat(another.plugins || []),
    optimization: another.optimization,
  };
};